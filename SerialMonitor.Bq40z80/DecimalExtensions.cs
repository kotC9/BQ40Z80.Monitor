namespace SerialMonitor.Bq40z80
{
    public static class DecimalExtensions
    {
        public static byte[] ToBytes(this ushort command) => new []{(byte) (command & 0xff), (byte) ((command >> 8) & 0xff)};
    }
}