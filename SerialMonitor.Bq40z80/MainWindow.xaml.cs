﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialMonitor.Bq40z80
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SerialPort _serialPort;
        private readonly MainViewModel _viewModel;
        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MainViewModel();
            DataContext = _viewModel;
            _viewModel.SerialPorts = new ObservableCollection<string>(SerialPort.GetPortNames());

            if (_viewModel.SerialPorts.Count > 0)
            {
                SerialPortComboBox.SelectedIndex = 0;
            }

            var serialMonitorTimer = new Timer(5000);
            serialMonitorTimer.Elapsed += (sender, args) =>
            {
                Dispatcher.Invoke(() =>
                {
                    var selectedItem = SerialPortComboBox.SelectedItem as string;
                    _viewModel.SerialPorts.Clear();
                    foreach (var port in SerialPort.GetPortNames())
                    {
                        _viewModel.SerialPorts.Add(port);
                    }

                    if (selectedItem != null)
                        SerialPortComboBox.SelectedItem = selectedItem;
                });
            };
            serialMonitorTimer.Start();
        }

        private void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                SerialMonitorTextBox.Text += _serialPort.ReadExisting();
                if (SerialMonitorTextBox.Text.Last() == 'z')
                    SerialMonitorTextBox.Text += $"\n{DateTime.Now}\n";
                SerialMonitorTextBox.ScrollToLine(SerialMonitorTextBox.LineCount - 1);
            });
        }

        private void SerialSafePrint(string data)
        {
            if (_serialPort is not {IsOpen: true})
                return;
            
            try
            {
                _serialPort.Write(data);
            }
            catch
            {
                MessageBox.Show($"Связь с {_serialPort.PortName} потеряна");
                _viewModel.Started = false;
            }
        }
        
        private void SerialSafePrintByte(byte[] data)
        {
            if (_serialPort is not {IsOpen: true})
                return;
            
            try
            {
                _serialPort.Write(data,0, data.Length);
            }
            catch
            {
                MessageBox.Show($"Связь с {_serialPort.PortName} потеряна");
                _viewModel.Started = false;
            }
        }

        private void SendCommand(ushort command)
        {

            // 109 = m
            var commandBytes = command.ToBytes();
            var result = new byte[] {109, commandBytes[0], commandBytes[1]};
            SerialSafePrintByte(result);
        }
        
        private void OnButtonShutdownMode_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0010);
        }

        private void OnButtonSleepMode_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0011);
        }

        private void OnButtonAutoCCOfset_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0013);
        }

        private void OnButtonPreDischargeFET_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x001C);
        }

        private void OnButtonFuseToggle_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x001D);
        }

        private void OnButtonPrechargeFET_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x001E);
        }

        private void OnButtonChargeFET_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x001F);
        }

        private void OnButtonDischargeFET_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0020);
        }

        private void OnButtonGauging_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0021);
        }

        private void OnButtonFETControl_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0022);
        }

        private void OnButtonLifetimeCollection_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0023);
        }

        private void OnButtonPermanentFailure_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0024);
        }

        private void OnButtonBackBoxRec_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0025);
        }

        private void OnButtonFuse_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0026);
        }

        private void OnButtonLEDDisplayEnable_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0027);
        }

        private void OnButtonLifetimeReset_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0028);
        }

        private void OnButtonPFailureReset_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0029);
        }

        private void OnButtonBlackBoxReset_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002A);
        }

        private void OnButtonLEDToggle_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002B);
        }

        private void OnButtonLEDDisplayPress_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002C);
        }

        private void OnButtonCalibrationMode_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002D);
        }

        private void OnButtonLifetimeFlush_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002E);
        }

        private void OnButtonLifetimeSpeedUp_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x002F);
        }

        private void OnButtonSeal_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0030);
        }

        private void OnButtonUnseal_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("u");
        }

        private void OnButtonReset_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x0041);
        }

        private void OnButtonIATA_SHUTDOWN_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x00F0);
        }

        private void OnButtonIATA_Charge_Clicked(object sender, RoutedEventArgs e)
        {
            SendCommand(0x00F3);
        }

        private void OnButtonSend_Clicked(object sender, RoutedEventArgs e)
        {
            // try convert
            try
            {
                var command = Convert.ToUInt16(ManualSenderTextBox.Text, 16);
                SendCommand(command);
            }
            catch
            {
                MessageBox.Show("invalid string");
                ManualSenderTextBox.Text = "";
            }
        }

        private void OnButtonRead_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("r");
        }

        private void OnButtonEnableCharge_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("bc");
        }

        private void OnButtonEnableDischarge_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("bd");
        }

        private void OnButtonEnablePredischarge_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("bp");
        }

        private void OnButtonEnableKbCharge_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("ke");
        }

        private void OnButtonDisableKbCharge_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("kd");
        }

        private void OnButtonDisableAll_Clicked(object sender, RoutedEventArgs e)
        {
            SerialSafePrint("bx");
        }

        private void OnButtonClear_Clicked(object sender, RoutedEventArgs e)
        {
            SerialMonitorTextBox.Text = "";
        }
        
        private void OnButtonConnect_Clicked(object sender, RoutedEventArgs e)
        {
            _serialPort = new SerialPort(SerialPortComboBox.SelectedItem as string, 115200)
            {
                Encoding = Encoding.ASCII,
                DataBits = 8,
                Handshake = Handshake.None,
                Parity = Parity.None,
                StopBits = StopBits.One
            };
            _serialPort.DataReceived += OnDataReceived;

            try
            {
                _serialPort.Open();
                _viewModel.Started = true;
            }
            catch
            {
                MessageBox.Show($"Не удалось открыть {_serialPort.PortName}");
                _viewModel.Started = false;
            }
        }
        
        private void OnButtonDisconnect_Clicked(object sender, RoutedEventArgs e)
        {
            if(_serialPort is {IsOpen: true})
                _serialPort.Close();
            
            _viewModel.Started = false;
            SerialMonitorTextBox.Text = "";
        }

        private void OnButtonFlashOnce_Clicked(object sender, RoutedEventArgs e)
        {
            try
            {
                var addressBytes = Convert.ToUInt16(FlashAddressTextBox.Text, 16).ToBytes();
                var valueBytes = Convert.ToUInt16(FlashValueTextBox.Text, 16).ToBytes();
                
                // f = 102
                var result = new byte[] {102, addressBytes[0], addressBytes[1], valueBytes[0], valueBytes[1]};
                SerialSafePrintByte(result);
            }
            catch
            {
                MessageBox.Show("invalid string");
                ManualSenderTextBox.Text = "";
            }
        }

        private void OnFlashFileDropped(object sender, DragEventArgs e)
        {
            
        }

        private void OnFlashFileClicked(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}