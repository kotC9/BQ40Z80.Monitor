using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SerialMonitor.Bq40z80.Annotations;

namespace SerialMonitor.Bq40z80
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private bool _started = false;
        private bool _stoped = true;

        public bool Started
        {
            get => _started;
            set
            {
                _started = value;
                OnPropertyChanged(nameof(Started));
                
                Stoped = !_started;
            }
        }

        public bool Stoped
        {
            get => _stoped;
            set
            {
                _stoped = value;
                OnPropertyChanged(nameof(Stoped));
            }
        }

        public ObservableCollection<string> SerialPorts { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}