

using System;
using System.IO;

namespace Bq40z80.ChemistryConverter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if(args.Length is > 2 or 0)
                Console.WriteLine("Invalid args count, exit");

            var chemistryRawFile = args[0];
            var chemistryOutputFile = args.Length > 1 ? args[1] : "chemistry";

            var convertedChemistry = ChemistryConverter.Parse(args[0]);
            
            // remove dot in path
            var correctOutput = chemistryOutputFile.Split('.')[0];
            
            File.WriteAllLines(correctOutput, convertedChemistry);
        }
    }
}